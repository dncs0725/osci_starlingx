https://docs.starlingx.io/deployment_guides/current/simplex.html


**starlingX**
    It's staringX source code for deploying on Centos

    [root@q352-4325 osci_starlingx]# ls
    20190407T233001Z-bootimage.iso  stx-tools    stxcloud.rc.bk  vms
    README.md                       stxcloud.rc  test
    [root@q352-4325 osci_starlingx]# SCRIPTS=$(pwd)/stx-tools/deployment/libvirt
    [root@q352-4325 libvirt]# $SCRIPTS/setup_network.sh
    [root@q352-4325 libvirt]# virsh list
    setlocale: No such file or directory
     Id    Name                           State
    ----------------------------------------------------
    
    [root@q352-4325 libvirt]# brctl show
    bridge name	bridge id		STP enabled	interfaces
    
    stxbr1		8000.000000000000	no
    stxbr2		8000.000000000000	no
    stxbr3		8000.000000000000	no
    stxbr4		8000.000000000000	no
    virbr0		8000.525400d657e8	yes		virbr0-nic
    
     sh setup_configuration.sh -c simplex -i ../../20190407T233001Z-bootimage.iso
     
     [root@q352-4325 libvirt]# virsh list
    setlocale: No such file or directory
     Id    Name                           State
    ----------------------------------------------------
     5     simplex-controller-0           running
    
    # virsh console 5
    
        type [ESC]
        
                 ┌────────────────────────────────────────────────────────┐
                  │         Select kernel options and boot kernel          │
                  ├────────────────────────────────────────────────────────┤
                  │ Standard Controller Configuration                    > │
                  │                                                        │
                  │ ## All-in-one Controller Configuration                  > │
                  │                                                        │
                  │ All-in-one (lowlatency) Controller Configuration     > │
        
              ┌────────────────────────────────────────────────────────┐
                  │          All-in-one Controller Configuration           │
                  ├────────────────────────────────────────────────────────┤
                  │ ## Serial Console                                       > │
                  │ Graphical Console                                    > │
                  
                           ┌────────────────────────────────────────────────────────┐
                  │                     Serial Console                     │
                  ├────────────────────────────────────────────────────────┤
                  │## STANDARD Security Boot Profile                         │
                  │ EXTENDED Security Boot Profile                         │
        
        
        Loading vmlinuz.........
        Loading initrd.img..........................................................ready.
        [    0.000000] Initializing cgroup subsys cpuset
        [    0.000000] Initializing cgroup subsys cpu
        [    0.000000] Initializing cgroup subsys cpuacct
        [    0.000000] Linux version 3.10.0-957.1.3.el7.1.tis.x86_64 (mockbuild@417d3c10fe7f) (gcc version 4.8.5 20150623 (Red Hat 4.8.5-36) (GCC) ) #1 SMP PREEMPT Mon Apr 8 00:10:32 UTC 2019
        [    0.000000] Command line: rootwait console=ttyS0,115200 inst.text serial inst.stage2=hd:LABEL=oe_iso_boot inst.ks=hd:LABEL=oe_iso_boot:/smallsystem_ks.cfg boot_device=sda rootfs_device=sda biosdevname=0 usbcore.autosuspend=-1 inst.gpt security_profile=standard user_namespace.enable=1 initrd=initrd.img BOOT_IMAGE=vmlinuz
        
        
        Start the script with no parameters:


================
controller-0:~$ sudo config_controller


Enter ! at any prompt to abort...
...

        
        controller-0:~$ source /etc/platform/openrc
        [wrsroot@controller-0 ~(keystone_admin)]$ system ntp-modify ntpservers=0.pool.ntp.org,1.pool.ntp.org
        
        [wrsroot@controller-0 ~(keystone_admin)]$ vi config_data_interface.sh
        
        DATA0IF=eth1000
        DATA1IF=eth1001
        export COMPUTE=controller-0
        PHYSNET0='physnet0'
        PHYSNET1='physnet1'
        SPL=/tmp/tmp-system-port-list
        SPIL=/tmp/tmp-system-host-if-list
        source /etc/platform/openrc
        system host-port-list ${COMPUTE} --nowrap > ${SPL}
        system host-if-list -a ${COMPUTE} --nowrap > ${SPIL}
        DATA0PCIADDR=$(cat $SPL | grep $DATA0IF |awk '{print $8}')
        DATA1PCIADDR=$(cat $SPL | grep $DATA1IF |awk '{print $8}')
        DATA0PORTUUID=$(cat $SPL | grep ${DATA0PCIADDR} | awk '{print $2}')
        DATA1PORTUUID=$(cat $SPL | grep ${DATA1PCIADDR} | awk '{print $2}')
        DATA0PORTNAME=$(cat $SPL | grep ${DATA0PCIADDR} | awk '{print $4}')
        DATA1PORTNAME=$(cat  $SPL | grep ${DATA1PCIADDR} | awk '{print $4}')
        DATA0IFUUID=$(cat $SPIL | awk -v DATA0PORTNAME=$DATA0PORTNAME '($12 ~ DATA0PORTNAME) {print $2}')
        DATA1IFUUID=$(cat $SPIL | awk -v DATA1PORTNAME=$DATA1PORTNAME '($12 ~ DATA1PORTNAME) {print $2}')
        # configure the datanetworks in sysinv, prior to referencing it in the 'system host-if-modify command'
        system datanetwork-add ${PHYSNET0} vlan
        system datanetwork-add ${PHYSNET1} vlan
        # the host-if-modify '-p' flag is deprecated in favor of  the '-d' flag for assignment of datanetworks.
        system host-if-modify -m 1500 -n data0 -d ${PHYSNET0} -c data ${COMPUTE} ${DATA0IFUUID}
        system host-if-modify -m 1500 -n data1 -d ${PHYSNET1} -c data ${COMPUTE} ${DATA1IFUUID}
        
        Run the shell script:
        [wrsroot@controller-0 ~(keystone_admin)]$ bash config_data_interface.sh
        
        [wrsroot@controller-0 ~(keystone_admin)]$ system host-if-list controller-0
        
        +--------------------------------------+-------+----------+----------+------+--------------+------+------+---------------------------+---------------+
        | uuid                                 | name  | class    | type     | vlan | ports        | uses | used | attributes                | data networks |
        |                                      |       |          |          | id   |              | i/f  | by   |                           |               |
        |                                      |       |          |          |      |              |      | i/f  |                           |               |
        +--------------------------------------+-------+----------+----------+------+--------------+------+------+---------------------------+---------------+
        | 44c3c629-ae79-4ee4-85ee-96ca41e46d93 | data1 | data     | ethernet | None | [u'eth1001'] | []   | []   | MTU=1500,accelerated=True | [u'physnet1'] |
        | 5a27687a-bac0-475a-b398-d1a728ee03bf | data0 | data     | ethernet | None | [u'eth1000'] | []   | []   | MTU=1500,accelerated=True | [u'physnet0'] |
        | 90a8bd9f-b2c3-465c-9958-e5c2c7ada628 | ens3  | platform | ethernet | None | [u'ens3']    | []   | []   | MTU=1500                  | []            |
        | 920eb5c5-0b26-4525-84ca-c3202abc0870 | lo    | platform | virtual  | None | []           | []   | []   | MTU=1500                  | []            |
        +--------------------------------------+-------+----------+----------+------+--------------+------+------+---------------------------+---------------+
        
        [wrsroot@controller-0 ~(keystone_admin)]$ kubectl get all
        NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
        service/kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   67m
        [wrsroot@controller-0 ~(keystone_admin)]$ kubectl get all -n kube-system
        NAME                                           READY   STATUS    RESTARTS   AGE
        pod/calico-kube-controllers-655ccb7b97-zvgft   1/1     Running   0          67m
        pod/calico-node-qh2rc                          1/1     Running   0          67m
        pod/calico-typha-65bdb998-s47vf                1/1     Running   0          67m
        pod/coredns-5c4849b47c-cx9gn                   0/1     Pending   0          67m
        pod/coredns-5c4849b47c-gx2m9                   1/1     Running   0          67m
        pod/kube-apiserver-controller-0                1/1     Running   0          66m
        pod/kube-controller-manager-controller-0       1/1     Running   0          66m
        pod/kube-proxy-hglhq                           1/1     Running   0          67m
        pod/kube-scheduler-controller-0                1/1     Running   0          66m
        pod/tiller-deploy-744cffbb75-hwwjg             1/1     Running   0          66m
        
        NAME                    TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)         AGE
        service/calico-typha    ClusterIP   10.108.72.119   <none>        5473/TCP        67m
        service/kube-dns        ClusterIP   10.96.0.10      <none>        53/UDP,53/TCP   67m
        service/tiller-deploy   ClusterIP   10.109.249.6    <none>        44134/TCP       66m
        
        NAME                         DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR                 AGE
        daemonset.apps/calico-node   1         1         1       1            1           beta.kubernetes.io/os=linux   67m
        daemonset.apps/kube-proxy    1         1         1       1            1           <none>                        67m
        
        NAME                                      READY   UP-TO-DATE   AVAILABLE   AGE
        deployment.apps/calico-kube-controllers   1/1     1            1           67m
        deployment.apps/calico-typha              1/1     1            1           67m
        deployment.apps/coredns                   1/2     2            1           67m
        deployment.apps/tiller-deploy             1/1     1            1           66m
        
        NAME                                                 DESIRED   CURRENT   READY   AGE
        replicaset.apps/calico-kube-controllers-655ccb7b97   1         1         1       67m
        replicaset.apps/calico-typha-65bdb998                1         1         1       67m
        replicaset.apps/coredns-5c4849b47c                   2         2         1       67m
        replicaset.apps/tiller-deploy-744cffbb75             1         1         1       66m
        
        [wrsroot@controller-0 ~(keystone_admin)]$  system host-label-assign controller-0 openstack-control-plane=enabled
        
        +-------------+--------------------------------------+
        | Property    | Value                                |
        +-------------+--------------------------------------+
        | uuid        | 765be3aa-6eb2-47dc-a90a-926dd0f79939 |
        | host_uuid   | 82a1262e-fd73-455a-903d-6cb7638be0dc |
        | label_key   | openstack-control-plane              |
        | label_value | enabled                              |
        +-------------+--------------------------------------+
        [wrsroot@controller-0 ~(keystone_admin)]$
        [wrsroot@controller-0 ~(keystone_admin)]$
        [wrsroot@controller-0 ~(keystone_admin)]$  system host-label-assign controller-0 openstack-compute-node=enabled
        
        +-------------+--------------------------------------+
        | Property    | Value                                |
        +-------------+--------------------------------------+
        | uuid        | 7b1ab1cd-07a1-462b-b147-8141adb05f75 |
        | host_uuid   | 82a1262e-fd73-455a-903d-6cb7638be0dc |
        | label_key   | openstack-compute-node               |
        | label_value | enabled                              |
        +-------------+--------------------------------------+
        [wrsroot@controller-0 ~(keystone_admin)]$
        [wrsroot@controller-0 ~(keystone_admin)]$  system host-label-assign controller-0 openstack-compute-node=enabled
        
        [wrsroot@controller-0 ~(keystone_admin)]$
        [wrsroot@controller-0 ~(keystone_admin)]$ system host-label-assign controller-0 sriov=enabled
        
        +-------------+--------------------------------------+
        | Property    | Value                                |
        +-------------+--------------------------------------+
        | uuid        | 51d86cb4-a134-4e3e-b65a-26a3bab132b0 |
        | host_uuid   | 82a1262e-fd73-455a-903d-6cb7638be0dc |
        | label_key   | sriov                                |
        | label_value | enabled                              |
        +-------------+--------------------------------------+
        [wrsroot@controller-0 ~(keystone_admin)]$
        [wrsroot@controller-0 ~(keystone_admin)]$ system host-label-assign controller-0 sriov=enabled
        
        [wrsroot@controller-0 ~(keystone_admin)]$
        [wrsroot@controller-0 ~(keystone_admin)]$ system host-label-assign controller-0 openvswitch=enabled
        +-------------+--------------------------------------+
        | Property    | Value                                |
        +-------------+--------------------------------------+
        | uuid        | c3a8e790-9631-40af-bfce-4e3d42f953c9 |
        | host_uuid   | 82a1262e-fd73-455a-903d-6cb7638be0dc |
        | label_key   | openvswitch                          |
        | label_value | enabled                              |
        +-------------+--------------------------------------+
        
        
        [wrsroot@controller-0 ~(keystone_admin)]$ cat setup_partitions.sh
        export COMPUTE=controller-0
        source /etc/platform/openrc
        
        echo ">>> Getting root disk info"
        ROOT_DISK=$(system host-show ${COMPUTE} | grep rootfs | awk '{print $4}')
        ROOT_DISK_UUID=$(system host-disk-list ${COMPUTE} --nowrap | grep ${ROOT_DISK} | awk '{print $2}')
        echo "Root disk: $ROOT_DISK, UUID: $ROOT_DISK_UUID"
        
        echo ">>>> Configuring nova-local"
        NOVA_SIZE=34
        NOVA_PARTITION=$(system host-disk-partition-add -t lvm_phys_vol ${COMPUTE} ${ROOT_DISK_UUID} ${NOVA_SIZE})
        NOVA_PARTITION_UUID=$(echo ${NOVA_PARTITION} | grep -ow "| uuid | [a-z0-9\-]* |" | awk '{print $4}')
        system host-lvg-add ${COMPUTE} nova-local
        system host-pv-add ${COMPUTE} nova-local ${NOVA_PARTITION_UUID}
        sleep 2
        
        echo ">>> Wait for partition $NOVA_PARTITION_UUID to be ready."
        while true; do system host-disk-partition-list $COMPUTE --nowrap | grep $NOVA_PARTITION_UUID | grep Ready; if [ $? -eq 0 ]; then break; fi; sleep 1; done
        
        echo ">>>> Extending cgts-vg"
        PARTITION_SIZE=6
        CGTS_PARTITION=$(system host-disk-partition-add -t lvm_phys_vol ${COMPUTE} ${ROOT_DISK_UUID} ${PARTITION_SIZE})
        CGTS_PARTITION_UUID=$(echo ${CGTS_PARTITION} | grep -ow "| uuid | [a-z0-9\-]* |" | awk '{print $4}')
        
        echo ">>> Wait for partition $CGTS_PARTITION_UUID to be ready"
        while true; do system host-disk-partition-list $COMPUTE --nowrap | grep $CGTS_PARTITION_UUID | grep Ready; if [ $? -eq 0 ]; then break; fi; sleep 1; done
        system host-pv-add ${COMPUTE} cgts-vg ${CGTS_PARTITION_UUID}
        sleep 2
        
        echo ">>> Waiting for cgts-vg to be ready"
        while true; do system host-pv-list ${COMPUTE} | grep cgts-vg | grep adding; if [ $? -ne 0 ]; then break; fi; sleep 1; done
        
        system host-pv-list ${COMPUTE}
        
        [wrsroot@controller-0 ~(keystone_admin)]$ sh setup_partitions.sh
        
        +--------------------------------------+-------------+--------------------------------------+--------------------------+--------------------------------------------------+--------------------+-----------+-------------+--------------------------------------+
        | uuid                                 | lvm_pv_name | disk_or_part_uuid                    | disk_or_part_device_node | disk_or_part_device_path                         | pv_state           | pv_type   | lvm_vg_name | ihost_uuid                           |
        +--------------------------------------+-------------+--------------------------------------+--------------------------+--------------------------------------------------+--------------------+-----------+-------------+--------------------------------------+
        | 19002067-da70-4939-8a5f-7df4bb410415 | /dev/sda4   | 434bb5f8-b532-4fc1-aebf-32142968148f | /dev/sda4                | /dev/disk/by-path/pci-0000:00:07.0-ata-1.0-part4 | provisioned        | partition | cgts-vg     | 82a1262e-fd73-455a-903d-6cb7638be0dc |
        | 5166be22-78ff-4abf-ac34-4d5a21a72dce | /dev/sda5   | a7e9f65e-4ff3-49d7-bb4f-79c652b0e32a | /dev/sda5                | /dev/disk/by-path/pci-0000:00:07.0-ata-1.0-part5 | adding (on unlock) | partition | nova-local  | 82a1262e-fd73-455a-903d-6cb7638be0dc |
        | 837f7ea6-c108-41bb-bf62-85bf6b36d1ac | /dev/sda6   | b09c816e-a059-4d38-9bb7-da4f6ba5c855 | /dev/sda6                | /dev/disk/by-path/pci-0000:00:07.0-ata-1.0-part6 | provisioned        | partition | cgts-vg     | 82a1262e-fd73-455a-903d-6cb7638be0dc |
        +--------------------------------------+-------------+--------------------------------------+--------------------------+--------------------------------------------------+--------------------+-----------+-------------+--------------------------------------+
        
        [wrsroot@controller-0 ~(keystone_admin)]$ vi config_ceph.sh
        
        source /etc/platform/openrc
        echo ">>> Enable primary Ceph backend"
        system storage-backend-add ceph --confirmed
         
        echo ">>> Wait for primary ceph backend to be configured"
        echo ">>> This step really takes a long time"
        while [ $(system storage-backend-list | awk '/ceph-store/{print $8}') != 'configured' ]; do echo 'Waiting for ceph..'; sleep 5; done
         
        echo ">>> Ceph health"
        ceph -s
         
        echo ">>> Add OSDs to primary tier"
         
        system host-disk-list controller-0
        system host-disk-list controller-0 | awk '/\/dev\/sdb/{print $2}' | xargs -i system host-stor-add controller-0 {}
        system host-stor-list controller-0
         
        echo ">>> ceph osd tree"
        ceph osd tree
        
        [wrsroot@controller-0 ~(keystone_admin)]$ sh config_ceph.sh
        >>> Enable primary Ceph backend
        
        System configuration has changed.
        Please follow the administrator guide to complete configuring the system.
        
        +--------------------------------------+------------+---------+-------------+------+----------+-------------------------------------+
        | uuid                                 | name       | backend | state       | task | services | capabilities                        |
        +--------------------------------------+------------+---------+-------------+------+----------+-------------------------------------+
        | 6221a882-9ee3-45b5-ae75-7c9fba667c4d | ceph-store | ceph    | configuring | {}   | None     | min_replication: 1 replication: 1   |
        |                                      |            |         |             |      |          |                                     |
        | d6317d04-e8c4-45f1-bd33-5b5f494b91ab | file-store | file    | configured  | None | glance   |                                     |
        +--------------------------------------+------------+---------+-------------+------+----------+-------------------------------------+
        
        
        >>> Add OSDs to primary tier
        +--------------------------------------+-----------+---------+---------+-------+------------+--------------+---------+--------------------------------------------+
        | uuid                                 | device_no | device_ | device_ | size_ | available_ | rpm          | serial_ | device_path                                |
        |                                      | de        | num     | type    | gib   | gib        |              | id      |                                            |
        +--------------------------------------+-----------+---------+---------+-------+------------+--------------+---------+--------------------------------------------+
        | a88eaf7e-f04d-42a0-9a36-1934d76e9d27 | /dev/sda  | 2048    | HDD     | 600.0 | 320.976    | Undetermined | QM00005 | /dev/disk/by-path/pci-0000:00:07.0-ata-1.0 |
        | b3946c39-5879-4b53-9824-702e6b6ccdd6 | /dev/sdb  | 2064    | HDD     | 200.0 | 199.997    | Undetermined | QM00007 | /dev/disk/by-path/pci-0000:00:07.0-ata-2.0 |
        | 1c8228e0-51b2-4d93-aef2-623bf781ec5a | /dev/sdc  | 2080    | HDD     | 200.0 | 199.997    | Undetermined | QM00009 | /dev/disk/by-path/pci-0000:00:07.0-ata-3.0 |
        +--------------------------------------+-----------+---------+---------+-------+------------+--------------+---------+--------------------------------------------+
        +------------------+--------------------------------------------------+
        | Property         | Value                                            |
        +------------------+--------------------------------------------------+
        | osdid            | 0                                                |
        | function         | osd                                              |
        | state            | configuring-on-unlock                            |
        | journal_location | 04ec691e-ba94-42d9-ad58-d7da13254bcd             |
        | journal_size_gib | 1024                                             |
        | journal_path     | /dev/disk/by-path/pci-0000:00:07.0-ata-2.0-part2 |
        | journal_node     | /dev/sdb2                                        |
        | uuid             | 04ec691e-ba94-42d9-ad58-d7da13254bcd             |
        | ihost_uuid       | 82a1262e-fd73-455a-903d-6cb7638be0dc             |
        | idisk_uuid       | b3946c39-5879-4b53-9824-702e6b6ccdd6             |
        | tier_uuid        | b1fe1cfa-4a8f-4000-bd9b-70a74f0b24c9             |
        | tier_name        | storage                                          |
        | created_at       | 2019-06-27T15:19:14.006637+00:00                 |
        | updated_at       | 2019-06-27T15:19:14.226085+00:00                 |
        +------------------+--------------------------------------------------+
        +--------------------------------------+----------+-------+-----------------------+--------------------------------------+--------------------------------------------------+--------------+------------------+-----------+
        | uuid                                 | function | osdid | state                 | idisk_uuid                           | journal_path                                     | journal_node | journal_size_gib | tier_name |
        +--------------------------------------+----------+-------+-----------------------+--------------------------------------+--------------------------------------------------+--------------+------------------+-----------+
        | 04ec691e-ba94-42d9-ad58-d7da13254bcd | osd      | 0     | configuring-on-unlock | b3946c39-5879-4b53-9824-702e6b6ccdd6 | /dev/disk/by-path/pci-0000:00:07.0-ata-2.0-part2 | /dev/sdb2    | 1                | storage   |
        +--------------------------------------+----------+-------+-----------------------+--------------------------------------+--------------------------------------------------+--------------+------------------+-----------+
        >>> ceph osd tree
        ID WEIGHT TYPE NAME                 UP/DOWN REWEIGHT PRIMARY-AFFINITY
        -1      0 root storage-tier
        -2      0     chassis group-0
        -3      0         host controller-0
         0      0 osd.0                        down        0          1.00000
         
         
         [wrsroot@controller-0 ~(keystone_admin)]$ ceph osd pool ls | xargs -i ceph osd pool set {} size 1
        set pool 0 size to 1
        
        [wrsroot@controller-0 ~(keystone_admin)]$  system host-unlock controller-0
         reboot / wait 10 min
         
        
        controller-0:/etc/platform$ source ./openrc
        [wrsroot@controller-0 platform(keystone_admin)]$
        [wrsroot@controller-0 platform(keystone_admin)]$ ceph -s
            cluster b0f9f398-3ffa-4560-881d-fd3bc5a62446
             health HEALTH_WARN
                    384 pgs degraded
                    384 pgs undersized
                    recovery 1116/2232 objects degraded (50.000%)
             monmap e1: 1 mons at {controller-0=192.168.204.3:6789/0}
                    election epoch 4, quorum 0 controller-0
             osdmap e19: 1 osds: 1 up, 1 in
                    flags sortbitwise,require_jewel_osds
              pgmap v26: 448 pgs, 7 pools, 1588 bytes data, 1116 objects
                    43748 kB used, 198 GB / 198 GB avail
                    1116/2232 objects degraded (50.000%)
                         384 active+undersized+degraded
                          64 active+clean
          client io 131 kB/s rd, 0 B/s wr, 196 op/s rd, 131 op/s wr
          
          [wrsroot@controller-0 platform(keystone_admin)]$ sudo su -
        Password:
        controller-0:~#
        controller-0:~#
        
        controller-0:~# wget -O helm-charts-manifest.tgz https://s3.ca-central-1.amazonaws.com/stx-workshop-artifacts/20190407T233001Z-helm-charts-manifest.tgz
        controller-0:~# source /etc/platform/openrc
        
        
        [root@controller-0 ~(keystone_admin)]#  system application-upload stx-openstack helm-charts-manifest.tgz
        +---------------+----------------------------------+
        | Property      | Value                            |
        +---------------+----------------------------------+
        | created_at    | 2019-06-27T15:34:36.989469+00:00 |
        | manifest_file | manifest.yaml                    |
        | manifest_name | armada-manifest                  |
        | name          | stx-openstack                    |
        | progress      | None                             |
        | status        | uploading                        |
        | updated_at    | None                             |
        +---------------+----------------------------------+
        Please use 'system application-list' or 'system application-show stx-openstack' to view the current progress.
        
        [wrsroot@controller-0 ~(keystone_admin)]$ system application-delete stx-openstack
        Application stx-openstack deleted.
        [wrsroot@controller-0 ~(keystone_admin)]$ system application-upload stx-openstack ../helm-charts-manifest.tgz
        Error: Tar file /home/wrsroot/../helm-charts-manifest.tgz does not exist
        [wrsroot@controller-0 ~(keystone_admin)]$ system application-upload stx-openstack /root/helm-charts-manifest.tgz
        
        [wrsroot@controller-0 ~(keystone_admin)]$ system application-list
        +---------------+-----------------+---------------+-----------+---------------------------------+
        | application   | manifest name   | manifest file | status    | progress                        |
        +---------------+-----------------+---------------+-----------+---------------------------------+
        | stx-openstack | armada-manifest | manifest.yaml | uploading | validating and uploading charts |
        +---------------+-----------------+---------------+-----------+---------------------------------+
        
        
        Every 2.0s: system application-show stx-openstack                    Thu Jun 27 16:32:59 2019
        
        +---------------+---------------------------------------------------------------------+
        | Property	| Value                                                               |
        +---------------+---------------------------------------------------------------------+
        | created_at    | 2019-06-27T16:11:55.428169+00:00                                    |
        | manifest_file | manifest.yaml                                                       |
        | manifest_name | armada-manifest                                                     |
        | name          | stx-openstack                                                       |
        | progress	| processing chart: osh-openstack-keystone, overall completion: 35.0% |
        | status        | applying                                                            |
        | updated_at    | 2019-06-27T16:32:48.447536+00:00                                    |
        +---------------+---------------------------------------------------------------------+
        
        
          
          







