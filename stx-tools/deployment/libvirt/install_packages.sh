#!/usr/bin/env bash
#
# SPDX-License-Identifier: Apache-2.0
#
# Copyright (C) 2019 Intel Corporation
#

# install_packages.sh - install required packages

sudo yum install qemu kvm qemu-kvm libvirt virt-install bridge-utils \
virt-manager dejavu-lgc-sans-fonts virt-viewer

#sudo yum install virt-manager libvirt qemu -y
#getenforce 0

#cat << EOF | sudo tee /etc/libvirt/qemu.conf
#user = "root"
#group = "root"
#EOF

sudo systemctl restart libvirtd
